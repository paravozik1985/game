<?php

namespace App\Factory;

use App\Entity\StrategyInterface;
use App\Entity\Type\PaperType;
use App\Entity\Type\ScissorsType;
use App\Entity\Type\StoneType;

class StrategyFactory
{
    public static function createPaperStrategy(): PaperType
    {
        return new PaperType();
    }

    public static function createRandomStrategy(): StrategyInterface
    {
        $types = [
            new PaperType(),
            new StoneType(),
            new ScissorsType(),
        ];

        return $types[array_rand($types)];
    }
}