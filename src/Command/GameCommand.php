<?php

namespace App\Command;

use App\Entity\GameResult;
use App\Entity\Player;
use App\Service\GameService;
use App\Factory\StrategyFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GameCommand extends Command
{
    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $playerA = (new Player('Player A'))->setStrategy(StrategyFactory::createPaperStrategy());
        $playerB = (new Player('Player B'))->setStrategy(StrategyFactory::createRandomStrategy());

        $service = new GameService($playerA, $playerB);

        for ($i = 0; $i < 100; ++$i) {
            $output->writeln(sprintf('>>> Round %d <<<', $i));

            $result = $service->run();

            $output->writeln($this->getResults($result));

            $playerB->setStrategy(StrategyFactory::createRandomStrategy());
        }

        return 0;
    }

    /**
     * @param GameResult $gameResult
     *
     * @return array
     */
    private function getResults(GameResult $gameResult): array
    {
        if (true === $gameResult->isDraw()) {
            return ['Draw'];
        }

        return [
            sprintf('Winner: %s', $gameResult->getWinner()->getName()),
            sprintf('Strategy: %s', $gameResult->getWinner()->getStrategy()->getName()),
            sprintf('Loser: %s', $gameResult->getLoser()->getName()),
            sprintf('Strategy: %s', $gameResult->getLoser()->getStrategy()->getName()),
        ];
    }
}