<?php

namespace App\Service;

use App\Entity\GameResult;
use App\Entity\Player;
use Symfony\Component\Console\Exception\InvalidArgumentException;

class GameService
{
    private Player $playerA;

    private Player $playerB;

    /**
     * GameService constructor.
     *
     * @param Player $playerA
     * @param Player $playerB
     */
    public function __construct(Player $playerA, Player $playerB)
    {
        if (null === $playerA->getStrategy()
            || null === $playerB->getStrategy()
        ) {
            throw new InvalidArgumentException('Set strategy for player');
        }
        $this->playerA = $playerA;
        $this->playerB = $playerB;
    }

    /**
     * @return GameResult
     */
    public function run(): GameResult
    {
        $result = new GameResult();

        $strategyA = $this->playerA->getStrategy();
        $strategyB = $this->playerB->getStrategy();

        if ($strategyA->morePriorityThan() === get_class($strategyB)) {
            $result
                ->setWinner($this->playerA)
                ->setLoser($this->playerB);
        } elseif ($strategyB->morePriorityThan() === get_class($strategyA)) {
            $result
                ->setWinner($this->playerB)
                ->setLoser($this->playerA);
        } else {
            $result->setDraw(true);
        }

        return $result;
    }
}