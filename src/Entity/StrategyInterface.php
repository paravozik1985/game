<?php

namespace App\Entity;

interface StrategyInterface
{
    public function morePriorityThan(): string;
    
    public function getName(): string;
}