<?php

namespace App\Entity;

interface PlayerInterface
{
    public function getName(): string;
}