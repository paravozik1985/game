<?php

namespace App\Entity;

class Player implements PlayerInterface
{
    public string $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @var StrategyInterface
     */
    public ?StrategyInterface $strategy = null;

    /**
     * @return StrategyInterface
     */
    public function getStrategy()
    {
        return $this->strategy;
    }

    /**
     * @param StrategyInterface $strategy
     *
     * @return self
     */
    public function setStrategy(StrategyInterface $strategy): self
    {
        $this->strategy = $strategy;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }
}