<?php

namespace App\Entity;

class GameResult
{
    private ?Player $winner = null;

    private ?Player $loser = null;

    private bool $draw = false;

    /**
     * @return Player|null
     */
    public function getWinner(): ?Player
    {
        return $this->winner;
    }

    /**
     * @param Player|null $winner
     *
     * @return self
     */
    public function setWinner(?Player $winner): self
    {
        $this->winner = $winner;

        return $this;
    }

    /**
     * @return Player|null
     */
    public function getLoser(): ?Player
    {
        return $this->loser;
    }

    /**
     * @param Player|null $loser
     *
     * @return self
     */
    public function setLoser(?Player $loser): self
    {
        $this->loser = $loser;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDraw(): bool
    {
        return $this->draw;
    }

    /**
     * @param bool $draw
     *
     * @return self
     */
    public function setDraw(bool $draw): self
    {
        $this->draw = $draw;

        return $this;
    }
}