<?php

namespace App\Entity\Type;

use App\Entity\StrategyInterface;

class ScissorsType implements StrategyInterface
{
    /**
     * @inheritDoc
     */
    public function morePriorityThan(): string
    {
        return PaperType::class;
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return 'Scissors';
    }
}