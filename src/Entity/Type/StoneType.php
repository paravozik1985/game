<?php

namespace App\Entity\Type;

use App\Entity\StrategyInterface;

class StoneType implements StrategyInterface
{
    /**
     * @inheritDoc
     */
    public function morePriorityThan(): string
    {
        return ScissorsType::class;
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return 'Stone';
    }
}