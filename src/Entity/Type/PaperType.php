<?php

namespace App\Entity\Type;

use App\Entity\StrategyInterface;

class PaperType implements StrategyInterface
{
    /**
     * @inheritDoc
     */
    public function morePriorityThan(): string
    {
        return StoneType::class;
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return 'Paper';
    }
}