<?php

namespace App\Tests;

use App\Entity\StrategyInterface;
use App\Entity\Type\PaperType;
use App\Factory\StrategyFactory;
use PHPUnit\Framework\TestCase;

class FactoryTest extends TestCase
{
    /**
     * @dataProvider randomProvider
     *
     * @param string $instance
     */
    public function testRandomStrategy(string $instance): void
    {
        $result = StrategyFactory::createRandomStrategy();
        $this->assertInstanceOf($instance, $result);
        $this->assertTrue(
            method_exists($result, 'getName'),
            'Class does not have method getName'
        );
        $this->assertTrue(
            method_exists($result, 'morePriorityThan'),
            'Class does not have method morePriorityThan'
        );
    }

    public function randomProvider(): array
    {
        return [
            [
                StrategyInterface::class
            ]
        ];
    }

    /**
     * @dataProvider paperProvider
     *
     * @param string $instance
     */
    public function testPaperStrategy(string $instance): void
    {
        $result = StrategyFactory::createPaperStrategy();
        $this->assertInstanceOf($instance, $result);
        $this->assertInstanceOf(StrategyInterface::class, $result);
        $this->assertTrue(
            method_exists($result, 'getName'),
            'Class does not have method getName'
        );
        $this->assertTrue(
            method_exists($result, 'morePriorityThan'),
            'Class does not have method morePriorityThan'
        );
    }

    public function paperProvider(): array
    {
        return [
            [
                PaperType::class
            ]
        ];
    }
}