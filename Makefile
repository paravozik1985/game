SHELL := /bin/bash
ALL: up
.PHONY: composer build up up-force down sh game test

composer:
	@docker-compose exec php-cli composer install --no-interaction

build:
	@docker-compose build --no-cache php-cli

up:
	@docker-compose up -d

up-force:
	@docker-compose up -d --force-recreate --build

down:
	@docker-compose down

sh:
	@docker-compose exec php-cli sh

game:
	@docker-compose exec php-cli php ./bin/console app:game

test:
	@docker-compose exec php-cli php ./bin/phpunit

